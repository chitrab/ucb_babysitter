import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ourfamily on 4/23/2015.
 */
public class BabySitter {
    public boolean checkValidityOfStartAndEndTime(String startTime){

        Calendar calendar1 = covertTimetoMilliSec(startTime);
        Calendar calendar2 = covertTimetoMilliSec("17:00:00");
        Calendar calendar3 = covertTimetoMilliSec("04:00:00");

        calendar3.add(Calendar.DATE, 1);

        if(calendar1.getTime().before(calendar2.getTime())){
            calendar1.add(Calendar.DATE, 1);
        }

        if(calendar1.getTime().after(calendar2.getTime()) && calendar1.getTime().before(calendar3.getTime())){
            return true;
        }

        return false;
    }

    public boolean checkStarttime(String startTime){
        Date time1 = null;
        try {
            time1 = new SimpleDateFormat("HH:mm:ss").parse(startTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date time2 = null;
        try {
            time2 = new SimpleDateFormat("HH:mm:ss").parse("17:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(time1.getTime() >= time2.getTime()){
            return true;
        }
        return false;
    }

    public boolean checkEndTimeLessThanEqualTo(String endTime, String endTimeRange){
        Date time1 = null;
        try {
            time1 = new SimpleDateFormat("HH:mm:ss").parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date time2 = null;
        try {
            time2 = new SimpleDateFormat("HH:mm:ss").parse(endTimeRange);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(time1.getTime() <= time2.getTime()){
            return true;
        }
        return false;
    }

    public long calculateRateBeforeBedTime(String startTime,String endTime){
        Date time1 = null;
        try {
            time1 = new SimpleDateFormat("HH:mm:ss").parse(startTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date time2 = null;
        try {
            time2 = new SimpleDateFormat("HH:mm:ss").parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long x = 0;
        if (((time2.getTime() - time1.getTime()) % (1000 * 60 * 60)) > 0){
            x = ((time2.getTime() - time1.getTime()) / (1000 * 60 * 60)) + 1;
        }
        else
            x = (time2.getTime() - time1.getTime()) / (1000 * 60 * 60);
        return x*12;
    }

    public long calculateRateUntilBedTime(String startTime){
        Date time1 = null;
        try {
            time1 = new SimpleDateFormat("HH:mm:ss").parse(startTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date time2 = null;
        try {
            time2 = new SimpleDateFormat("HH:mm:ss").parse("22:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long x = 0;
        if (((time2.getTime() - time1.getTime()) % (1000 * 60 * 60)) > 0){
            x = ((time2.getTime() - time1.getTime()) / (1000 * 60 * 60)) + 1;
        }
        else
            x = (time2.getTime() - time1.getTime()) / (1000 * 60 * 60);
        return x*12;
    }

    public long calculateRateBetweenMidnightUntilEnd(String endTime){
        Date time1 = null;
        try {
            time1 = new SimpleDateFormat("HH:mm:ss").parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date time2 = null;
        try {
            time2 = new SimpleDateFormat("HH:mm:ss").parse("00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long x = 0;
        if(((time1.getTime()- time2.getTime())%(1000*60*60)) > 0 ) {
            x = ((time1.getTime()- time2.getTime())/(1000*60*60)) + 1;
        }
        else
            x = (time1.getTime()- time2.getTime())/(1000*60*60);

        return x*16;
    }

    public long calculateRate(String startTime, String endTime){
        long totalRate = 0;
        if (checkStarttime(startTime)){
            if (checkEndTimeLessThanEqualTo(endTime,"04:00:00")){
                totalRate = calculateRateUntilBedTime(startTime);
                totalRate += 2*8;
                totalRate += calculateRateBetweenMidnightUntilEnd(endTime);
            }
            if ((checkEndTimeLessThanEqualTo(endTime,"23:59:59")) && !(checkEndTimeLessThanEqualTo(endTime,"22:00:00"))){
                totalRate = calculateRateUntilBedTime(startTime);
                if((checkEndTimeLessThanEqualTo(endTime,"23:00:00"))) {
                    totalRate += 8;
                }
                else
                totalRate += 2*8;
            }
            if (checkEndTimeLessThanEqualTo(endTime,"22:00:00")){
                totalRate = calculateRateBeforeBedTime(startTime,endTime);
            }

        }
        return totalRate;
    }

    public Calendar covertTimetoMilliSec(String aTime){
        Date time = null;
        try {
            time = new SimpleDateFormat("HH:mm:ss").parse(aTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        return calendar;
    }


}
