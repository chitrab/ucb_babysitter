import org.junit.Test;

import java.text.ParseException;

import static org.junit.Assert.*;

/**
 * Created by ourfamily on 4/23/2015.
 */
public class BabySitterTest {
    BabySitter babySitter;
    @org.junit.Before
    public void setUp() throws Exception {
        babySitter = new BabySitter();
    }

    @Test
    public void checkValidityofStartTimeAndEndTimeofBabySitter(){
        assertEquals(true, babySitter.checkValidityOfStartAndEndTime("19:00:00"));
        assertEquals(true,babySitter.checkValidityOfStartAndEndTime("01:00:00"));
        assertEquals(false,babySitter.checkValidityOfStartAndEndTime("14:00:00"));
        assertEquals(false,babySitter.checkValidityOfStartAndEndTime("06:00:00"));
    }

    @Test
    public void checkStartTimeofBabySitter(){
        assertEquals(true,babySitter.checkStarttime("18:00:00"));
    }

    @Test
     public void EndTimeLessThanOfBabySitterTest1(){
        assertEquals(true,babySitter.checkEndTimeLessThanEqualTo("21:00:00", "22:00:00"));
    }

    @Test
    public void EndTimeLessThanOfBabySitterTest2(){
        assertEquals(true,babySitter.checkEndTimeLessThanEqualTo("23:00:00","23:59:59"));
    }

    @Test
    public void EndTimeLessThanOfBabySitterTest3(){
        assertEquals(true,babySitter.checkEndTimeLessThanEqualTo("02:00:00","04:00:00"));
    }
    @Test
    public void calculateRateofBabySitterUntilBedtimeTest1(){
            assertEquals(60, babySitter.calculateRateUntilBedTime("17:00:00"));
    }
    @Test
    public void calculateRateofBabySitterUntilBedtimeTest2(){
        assertEquals(60, babySitter.calculateRateUntilBedTime("17:30:00"));
    }

    @Test
    public void calculateRateofBabySitterBetweenMidnightUntilEndTest1(){
        assertEquals(48, babySitter.calculateRateBetweenMidnightUntilEnd("03:00:00"));
    }

    @Test
    public void calculateRateofBabySitterBetweenMidnightUntilEndTest2(){
        assertEquals(48, babySitter.calculateRateBetweenMidnightUntilEnd("02:30:00"));
    }

    @Test
    public void calculateRateofBabySitterBeforeBedTime(){
        assertEquals(48, babySitter.calculateRateBeforeBedTime("17:00:00","21:00:00"));
    }


    @Test
    public void calculateRateofBabySitterTest1(){
        assertEquals(124,babySitter.calculateRate("17:00:00", "03:00:00"));
    }

    @Test
    public void calculateRateofBabySitterTest2(){
        assertEquals(76,babySitter.calculateRate("17:00:00","23:15:00"));
    }

    @Test
    public void calculateRateofBabySitterTest3(){
        assertEquals(68,babySitter.calculateRate("17:00:00","22:45:00"));
    }

    @Test
    public void calculateRateofBabySitterTest4(){
        assertEquals(48,babySitter.calculateRate("17:00:00","21:00:00"));
    }

    @org.junit.After
    public void tearDown() throws Exception {

    }
}